/*
 * grunt-url-crawler
 * 
 *
 * Copyright (c) 2013 Filip van Harreveld
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('url_crawler', 'Crawl your files for URLs and store them in a local file.', function() {
    var options = this.options({
      includeFileSource: '.'
    });

    var matchedURLarray = [];
    // Iterate over all specified file groups.
    this.files.forEach(function(f) {

      var src = f.src.filter(function(filepath) {
        // Warn on and remove invalid source files (if nonull was set).
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          return true;
        }
      }).map(function(filepath) {
        var content =  grunt.file.read(filepath);
        grunt.log.warn("READ FILE: " + filepath);
        if ( filepath.indexOf(".css" ) > 0 || filepath.indexOf(".scss" ) > 0 ) {
          matchedURLarray = matchURLS_CSS(content,filepath,options.includeFileSource);
          grunt.log.warn("images found in CSS: " + matchedURLarray);
        } else if ( filepath.indexOf(".html") > 0 ){
          matchedURLarray = matchURLS_HTML(content,filepath,options.includeFileSource);
          grunt.log.warn("images found in HTML: " + matchedURLarray);
        }
        // Read file source.
        return grunt.file.read(filepath);
      });

      // Handle options.
      src = '{ "imageURLS": ['+matchedURLarray+'] }';

      // Write the destination file.
      grunt.file.write(f.dest, src);

      // Print a success message.
      grunt.log.writeln('File "' + f.dest + '" created.');
    });
  });


  function matchURLS_CSS (_sourceTxt,_filepath,_includeSource) {
    var matches = String(_sourceTxt).match(/url\((['" ])?(.+?)\1\)/g);
    var imgUrls = [];
    var url;
      grunt.log.warn("CHECK URLS IN CSS");
      if (matches && matches.length > 0){
        var l = matches.length;
          //check if file is css of html
          for (var i=0;i<l;i++){
            if (matches[i].indexOf('"') > 0 ) {
              url = String(matches[i]).substring(5,String(matches[i]).length-2);
              //imgUrls.push('"' + url + '"');
            } else if (matches[i].indexOf("'") > 0 ) {
              url = String(matches[i]).substring(5,String(matches[i]).length-2);
              //imgUrls.push('"' + url + '"');
            } else {
              url = String(matches[i]).substring(4,String(matches[i]).length-2);
            }
            
            if (_includeSource) {
              imgUrls.push('{"' + url + "," + "'" + _filepath +'"}');
            } else {
              imgUrls.push('"' + url + '"' );
            }

          }
        }
      
      return imgUrls;    
  }

  function matchURLS_HTML (_sourceTxt,_filepath,_includeSource) {
    var matches = String(_sourceTxt).match(/<img[^>]+src="(http:\/\/[^">]+)"/g);
    var imgUrls = [];
    var url;
      grunt.log.warn("CHECK URLS IN HTML");
      if (matches && matches.length > 0){
        var l = matches.length;
          for (var i=0;i<l;i++){
            url = String(String(matches[i].match(/src=["'](.+?)["']/g)).substring(5,String(matches[i].match(/src=["'](.+?)["']/g)).length-1));
            if (url.indexOf("http://graph.facebook.com/") === -1) {
              if (_includeSource) {
                imgUrls.push('{"' + url + "," + "'" + _filepath +'"}');
                } else {
                  imgUrls.push('"' + url + '"' );
                }
              }
        }
      }

      return imgUrls;     
  }



};
