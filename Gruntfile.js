/*
 * grunt-url-crawler
 * 
 *
 * Copyright (c) 2013 Filip van Harreveld - MagicBullet - www.magicbullet.nl
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js',
        '<%= nodeunit.tests %>',
      ],
      options: {
        jshintrc: '.jshintrc',
      },
    },

    clean: {
      tests: ['result'],
    },

    url_crawler: {
      default_options: {
        options: {
          includeFileSource: true
        },
        files: {
          'result/CSSImages.JSON': ['test/fixtures/_main.scss'],
          'result/HTMLImages.JSON': ['test/fixtures/main.html']
        },
      }
    },

    // Unit tests.
    nodeunit: {
      tests: ['test/*_test.JSON'],
    },

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');

  // Whenever the "test" task is run, first clean the "tmp" dir, then run this
  // plugin's task(s), then test the result.
  grunt.registerTask('test', ['clean', 'url_crawler']);

  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'test']);

};
