# grunt-url-image-crawler

> Crawl your CSS/SCSS or HTML files for img URL's and store the crawled image URL's in a local JSON file.

## Getting Started
This plugin requires Grunt `~0.4.2`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-url-image-crawler --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-url-image-crawler');
```

## The "url_crawler" task

### Overview
In your project's Gruntfile, add a section named `url_crawler` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
    url_crawler: {
      default_options: {
        options: {
        },
        files: {
          'result/CSSImages.JSON': ['test/fixtures/main.css'],
        },
      }
    }
});
```


### Usage Examples

#### Default Options
In this example test/fixtures/_main.scss and test/fixtures/main.html are being crawled and url() and img images are being extracted and saved to a local JSON file result/CSSImages.JSON and result/HTMLImages.JSON
use includeFileSource: true to insert in the array also the path to the file where the url's are crawled from. 

```js
    url_crawler: {
      default_options: {
        options: {
          includeFileSource: true
        },
        files: {
          'result/CSSImages.JSON': ['test/fixtures/_main.scss'],
          'result/HTMLImages.JSON': ['test/fixtures/main.html']
        },
      }
    }
```


## Release History
1.1.1 - check for use of quotes in url() in css

1.1.2 - grunt logs cleaned up

1.1.3 - readme cleaned up, def options removed

1.1.5 - now also supports img tags from HTML files

1.3.0 - option to include the source filepath to the output array